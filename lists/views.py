from django.shortcuts import redirect, render
from lists.models import Item, List
from random import randint

def home_page(request):
    return render(request, 'home.html')

def new_list(request):
    list_ = List.objects.create()

    rand = randint(1,30)
    nilaitebakan = int(request.POST['item_text'])
    hasil = cek(rand, nilaitebakan)
    Item.objects.create(tebakan=nilaitebakan, random=rand, status=hasil, list=list_)
    return redirect('/lists/%d/' % (list_.id,))

def add_item(request, list_id):
    list_ = List.objects.get(id=list_id)
    rand = randint(1,30)
    nilaitebakan = int(request.POST['item_text'])
    hasil = cek(rand, nilaitebakan)
    Item.objects.create(tebakan=nilaitebakan, random=rand, status=hasil, list=list_)
    return redirect('/lists/%d/' % (list_.id,))

def cek(random, tebakan):
    if (tebakan >= (random-2))and(tebakan <= (tebakan+2)):
        return 'kamu menang'
    else:
        return 'kalah'

def view_list(request, list_id):
    list_ = List.objects.get(id=list_id)
    komentar = statkomen(list_)
    return render(request, 'list.html', {'list': list_, 'komentar': komentar})

def statkomen(list_):
    list = Item.objects.filter(list=list_).reverse()
    counter = 0
    komentar =''

    if len(list) < 3:
        return komentar
    
    for i in range(3):
        item = list[i]
        if item.status == 'kamu menang':
            counter = counter + 1

    if counter == 3:
        komentar = 'Ulala!!'

    counter = 0

    if len(list) < 5:
        return komentar
    
    for i in range(5):
        item = list[i]
        if item.status == 'kamu menang':
            counter = counter + 1

    if counter == 5:
        komentar = 'Ulalala'

    counter = 0   
    
    if len(list) < 10:
        return komentar
    
    for i in range(10):
        item = list[i]
        if item.status == 'kamu menang':
            counter = counter + 1

    if counter == 10:
        komentar = 'Uulala'

    return komentar


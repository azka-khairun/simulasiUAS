from django.db import models

# Create your models here.
class List(models.Model):
    pass
class Item(models.Model):
    status = models.TextField(default='')
    tebakan = models.IntegerField(default=0)
    random = models.IntegerField(default=0)
    list = models.ForeignKey(List, default=None)

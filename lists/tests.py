from django.core.urlresolvers import resolve
from django.test import TestCase
from django.http import HttpRequest
from django.template.loader import render_to_string

from lists.models import Item, List
from lists.views import home_page

class TestingSimulasi(TestCase):

#b
    def test_displays_only_items_for_that_list(self):
        correct_list = List.objects.create()
        Item.objects.create(tebakan=12, random=14, status='kamu menang', list=correct_list)
        Item.objects.create(tebakan=3, random=15, status='kalah', list=correct_list)

        response = self.client.get('/lists/%d/' % (correct_list.id,))

        self.assertContains(response, 'kamu menang')
        self.assertContains(response, 'kalah')
        self.assertContains(response, '12')
        self.assertContains(response, '14')
        self.assertContains(response, '3')
        self.assertContains(response, '15')

#c
    def test_passes_correct_list_to_template(self):
        other_list = List.objects.create()
        correct_list = List.objects.create()
        response = self.client.get('/lists/%d/' % (correct_list.id,))
        self.assertEqual(response.context['list'], correct_list)

#a
    def test_saving_a_POST_request(self):
        self.client.post(
            '/lists/new',
            data={'item_text': '12'}
        )
        self.assertEqual(Item.objects.count(), 1)
        new_item = Item.objects.first()
        self.assertEqual(new_item.tebakan, 12)

#d
    def test_can_save_a_POST_request_to_an_existing_list(self):
        correct_list = List.objects.create()
        Item.objects.create(tebakan=15, random=17, status='kamu menang', list=correct_list)

        self.client.post(
            '/lists/%d/add_item' % (correct_list.id,),
            data={'item_text': '13'}
        )

        response = self.client.get('/lists/%d/' % (correct_list.id,))

        self.assertEqual(Item.objects.count(), 2)
        self.assertContains(response, 'kamu menang')
        self.assertContains(response, '15')
        self.assertContains(response, '17')

#e
    def test_record_permainan(self):
        correct_list = List.objects.create()
        other_list = List.objects.create()
        Item.objects.create(tebakan=15, random=17, status='kamu menang', list=correct_list)

        Item.objects.create(tebakan=10, random=14, status='kalah', list=other_list)


        response = self.client.get('/lists/%d/' % (correct_list.id))

        self.assertContains(response, '15')
        self.assertContains(response, '17')
        self.assertContains(response, 'kamu menang')
        self.assertNotContains(response, '10')
        self.assertNotContains(response, '14')
        self.assertNotContains(response, 'kalah')

    def test_permainan_dua(self):
        correct_list = List.objects.create()
        Item.objects.create(tebakan=11, random=1, status='kalah', list=correct_list)        
        Item.objects.create(tebakan=11, random=11, status='kalah', list=correct_list) 
        response = self.client.get('/lists/%d/' % (correct_list.id))
        self.assertNotContains(response, 'Ulala!!')
        self.assertNotContains(response, 'Ulalala')
        self.assertNotContains(response, 'Uulala')

    def test_permainan_tiga(self):
        correct_list = List.objects.create()
        Item.objects.create(tebakan=15, random=17, status='kamu menang', list=correct_list)       
        Item.objects.create(tebakan=15, random=17, status='kamu menang', list=correct_list)
        Item.objects.create(tebakan=15, random=17, status='kamu menang', list=correct_list)
        response = self.client.get('/lists/%d/' % (correct_list.id))
        self.assertContains(response, 'Ulala!!')
        self.assertNotContains(response, 'Ulalala')
        self.assertNotContains(response, 'Uulala')

    def test_permainan_lima(self):
        correct_list = List.objects.create()
        Item.objects.create(tebakan=15, random=17, status='kamu menang', list=correct_list)       
        Item.objects.create(tebakan=15, random=17, status='kamu menang', list=correct_list)
        Item.objects.create(tebakan=15, random=17, status='kamu menang', list=correct_list)
        Item.objects.create(tebakan=15, random=17, status='kamu menang', list=correct_list)
        Item.objects.create(tebakan=15, random=17, status='kamu menang', list=correct_list)
        response = self.client.get('/lists/%d/' % (correct_list.id))
        self.assertNotContains(response, 'Ulala!!')
        self.assertContains(response, 'Ulalala')
        self.assertNotContains(response, 'Uulala')

    def test_permainan_sepuluh(self):
        correct_list = List.objects.create()
        Item.objects.create(tebakan=15, random=17, status='kamu menang', list=correct_list)       
        Item.objects.create(tebakan=15, random=17, status='kamu menang', list=correct_list)
        Item.objects.create(tebakan=15, random=17, status='kamu menang', list=correct_list)
        Item.objects.create(tebakan=15, random=17, status='kamu menang', list=correct_list)
        Item.objects.create(tebakan=15, random=17, status='kamu menang', list=correct_list)
        Item.objects.create(tebakan=15, random=17, status='kamu menang', list=correct_list)       
        Item.objects.create(tebakan=15, random=17, status='kamu menang', list=correct_list)
        Item.objects.create(tebakan=15, random=17, status='kamu menang', list=correct_list)
        Item.objects.create(tebakan=15, random=17, status='kamu menang', list=correct_list)
        Item.objects.create(tebakan=15, random=17, status='kamu menang', list=correct_list)
        response = self.client.get('/lists/%d/' % (correct_list.id))
        self.assertNotContains(response, 'Ulala!!')
        self.assertNotContains(response, 'Ulalala')
        self.assertContains(response, 'Uulala')
